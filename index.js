var express = require('express')
var app = express()
const context = require('./context');

app.use(context.setup);

app.get('/', function(req, res) {
    console.log(context.current())
    res.send('Hello World!')
})

app.listen(3000, function() {
    console.log('Example app listening on port 3000!')
})

//make it work with connect and swagger

// logLevel, corID, namespace, datetime, messageLog, uri, headers,
// remote_host, remote_port, method, host,