
# Sample debugging with docker

once the app is up and running select `Attach to process` in visual studio code editor and click on the debug icon

```
docker build -t delme
docker run --rm -v $(pwd):/app -p 3000:3000 -p 5858:5858 -it delme /bin/bash
> node --inspect=0.0.0.0:5858 index.js
```