FROM node:8

COPY package.json /tmp/
WORKDIR /tmp/

VOLUME /app

RUN npm install && npm install -g nodemon && mv node_modules ../app

WORKDIR /app

EXPOSE 3000 5858




